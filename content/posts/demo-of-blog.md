+++
title = "demo of blog"
author = ["debin"]
date = 2017-05-13T16:24:00+08:00
draft = false
+++

basic demo of blog content supported by hugo.  

<!--more-->


## source code {#source-code}

-   example of verilog code

<!--listend-->

```verilog
// verilog comments;
assign io_outputs_0_r_ready = io_input_r_ready;
    assign io_outputs_1_r_ready = io_input_r_ready;
    assign io_outputs_2_r_ready = io_input_r_ready;
    always @ (posedge clk or negedge resetn) begin
      if (!resetn) begin
	pendingCmdCounter_value <= (3'b000);
	pendingSels <= (3'b000);
	pendingError <= 1'b0;
      end else begin
	pendingCmdCounter_value <= pendingCmdCounter_valueNext;
	if(io_input_ar_ready)begin
	  pendingSels <= decodedCmdSels;
	end
	if(io_input_ar_ready)begin
	  pendingError <= decodedCmdError;
	end
      end
    end

  endmodule   
```

-   example of python script

<!--listend-->

```python { linenos=table, linenostart=1 }
def Stream(fileName,NameList):
  f = open(fileName)
  Cnt=0 # comments for the python
  NList = NameList
  for line in f.readlines():
      m0 = re.search(r"(\w+)",line,re.M|re.I)
      if m0:     
```

-   example of peasudo code:

<!--listend-->

```text { linenos=table, linenostart=1 }
WRITE 'Input the limit'
READ N
FOR i = 1 TO N DO
  nIF (i = 1)    
```


## image link {#image-link}

{{< figure src="/ox-hugo/ddr_tWTR.png" caption="<span class=\"figure-number\">Figure 1: </span>figure1: ddr tWTR timing diagram" >}}  

{{< figure src="/ox-hugo/ddr_tRRD.png" caption="<span class=\"figure-number\">Figure 2: </span>figure2: ddr tRRD timing diagram" >}}  


## tables {#tables}

<a id="table--tab:1"></a>
<div class="table-caption">
  <span class="table-number"><a href="#table--tab:1">Table 1</a></span>:
  table1: summary of salary
</div>

| index | name | salary |
|-------|------|--------|
| 1     | John | 5,000  |
| 2     | Tom  | 6,500  |


## equations {#equations}

-   simple equation

my equation like below:  

\begin{equation}  
\label{eq:1}
C = W\log\_{2} (1+\mathrm{SNR})
\end{equation}

be aware about the previous issues: (related with emacs indent, which may affect final mathjax export);  
<https://github.com/kaushalmodi/ox-hugo/issues/128>  


## footnotes {#footnotes}

I have more[^fn:1] to say.  


## Todo lists {#todo-lists}

-   [X] item1
-   [ ] item2
-   [ ] item3


## important,notes,warning,quote {#important-notes-warning-quote}

> here's the quote!  

<div class="important">

here's an important!

</div>

<div class="note">

Here's notes!

</div>

<div class="warning">

Here's a warning!

</div>


## code/vertamin {#code-vertamin}

-   `x=y+z^2` is a new result;
-   key parameter `nWords` is 0x3 by default;


## bold/italic/strikethrough/underline {#bold-italic-strikethrough-underline}

-   **bold** is like this;
-   _italic_ also supported;
-   ~~strikethrough~~ may needed;
-   <span class="underline">underline</span> is OK;


## Chinese fonts {#chinese-fonts}

君自故乡来，应知故乡事。  
来日绮窗前，寒梅著花未。－－（唐）王维  


## internal links {#internal-links}

equation `\ref{eq:1}` referenced as \ref{eq:1}.  

[^fn:1]: footnote description here.