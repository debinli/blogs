+++
title = "Hugo for blogger"
author = ["debin"]
date = 2019-03-13T15:38:00+08:00
draft = false
+++

basic installation guideline for hugo.  

<!--more-->

-   installation on centos7  
    update centos7 repos, `sudo vi /etc/yum.repos.d/hugo.repo`  
    
    ```text
    [daftaupe-hugo]
    name=Copr repo for hugo owned by daftaupe
    baseurl=https://copr-be.cloud.fedoraproject.org/results/daftaupe/hugo/epel-7-$basearch/
    type=rpm-md
    skip_if_unavailable=True
    gpgcheck=1
    gpgkey=https://copr-be.cloud.fedoraproject.org/results/daftaupe/hugo/pubkey.gpg
    repo_gpgcheck=0
    enabled=1
    ```
    
    refer to: <https://unixetc.com/post/centos-install-hugo-via-repo/>
-   setup a new site called "base"  
    
    ```text
    ➜  blogger hugo new site base
    Congratulations! Your new Hugo site is created in /var/vobs/blogger/base.
    
    Just a few more steps and you're ready to go:
    
    1. Download a theme into the same-named folder.
    Choose a theme from https://themes.gohugo.io/ or
    create your own with the "hugo new theme <THEMENAME>" command.
    2. Perhaps you want to add some content. You can add single files
    with "hugo new <SECTIONNAME>/<FILENAME>.<FORMAT>".
    3. Start the built-in live server via "hugo server".
    
    Visit https://gohugo.io/ for quickstart guide and full documentation.
    ```
-   initial file structure  
    
    ```text
    ➜  blogger tree base
    base
    ├── archetypes
    │   └── default.md
    ├── config.toml
    ├── content
    ├── data
    ├── layouts
    ├── static
    └── themes
    
    6 directories, 2 files
    ```
-   download theme  
    
    ```text
    git submodule add https://github.com/ribice/kiss.git themes/kiss
    ```
-   create a new draft post  
    
    ```text
    ➜  base hugo new post/readme.md
    /var/vobs/blogger/base/content/post/readme.md created
    ➜  base cat content/post/readme.md 
    ---
    title: "Readme"
    date: 2020-02-16T09:50:29+08:00
    draft: true
    ---
    ```
-   update config.toml file,  
    default config.toml like:  
    
    ```text
    baseURL = "http://example.org/"
    languageCode = "en-us"
    title = "My New Hugo Site"
    ```
    
    according to the example, template from [mainroad](https://github.com/vimux/mainroad);
-   run hugo serve to preview the blogger  
    
    ```text
    ➜  base hugo server --buildDrafts                                                                        
                       | EN  
    -------------------+-----
      Pages            |  8  
      Paginator pages  |  0  
      Non-page files   |  0  
      Static files     |  7  
      Processed images |  0  
      Aliases          |  3  
      Sitemaps         |  1  
      Cleaned          |  0  
    
    Built in 23 ms
    Watching for changes in /var/vobs/blogger/base/{archetypes,content,data,layouts,static,themes}
    Watching for config changes in /var/vobs/blogger/base/config.toml
    Environment: "development"
    Serving pages from memory
    Running in Fast Render Mode. For full rebuilds on change: hugo server --disableFastRender
    Web Server is available at http://localhost:1313/ (bind address 127.0.0.1)
    Press Ctrl+C to stop
    ```
-   previewed website from address <http://localhost:1313/>
-   publish to public  
    
    ```text
    hugo -t <theme>
    ```
    
    with this command, there will generate a `public/` folder,  
    which expected to be deployed on remote server.